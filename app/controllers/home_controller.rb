class HomeController < ApplicationController
  def index
    @temperature_adapter = TemperatureAdapter.new(value: 0, type: 'CelsiusTemperature')
  end

  def convert
    @temperature_adapter = TemperatureAdapter.new(permitted_params)
    @temperature = @temperature_adapter.temperature
    if @temperature.valid?
      @notice = "#{@temperature.value_text} is #{@temperature.converted_value_text}"
    else
      @error = "Please enter the proper values"
    end
    render :index
  end

  private

  def permitted_params
    params.require(:temperature_adapter).permit(:value, :type, :convert_to)
  end
end
