class TemperatureAdapter
  ALLOWED_TYPES = ['CelsiusTemperature', 'FarenheitTemperature']

  include ActiveModel::Model

  attr_accessor :temperature, :value, :type, :convert_to

  def initialize(attrs)
    raise ArgumentError, 'Invalid Temperature Type' unless ALLOWED_TYPES.include?(attrs[:type])
    @temperature = attrs[:type].constantize.new(type: attrs[:type], value: attrs[:value],
                                                convert_to: attrs[:convert_to])
    super
  end
end
