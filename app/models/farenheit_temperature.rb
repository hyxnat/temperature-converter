class FarenheitTemperature < Temperature
  def to_celsius
    temp = (value.to_f - 32) * (5.0/9.0)
    CelsiusTemperature.new(value: temp.round(2))
  end

  def value_text
    "#{value} &deg; F"
  end

  def converted_value_text
    send(convert_to).value_text
  end
end
