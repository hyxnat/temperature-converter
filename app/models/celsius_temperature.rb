class CelsiusTemperature < Temperature
  def to_farenheit
    temp = (value.to_f * (9.0/5.0)) + 32
    FarenheitTemperature.new(value: temp.round(2))
  end

  def value_text
    "#{value} &deg; C"
  end

  def converted_value_text
    send(convert_to).value_text
  end
end
