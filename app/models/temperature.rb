class Temperature
  include ActiveModel::Model

  ALLOWED_CONVERSIONS = ['to_farenheit', 'to_celsius']

  attr_accessor :value, :type, :convert_to

  validates_presence_of :value, :type
  validates_presence_of :convert_to, allow_nil: true
  validates_inclusion_of :convert_to, in: ALLOWED_CONVERSIONS, allow_nil: true
end
