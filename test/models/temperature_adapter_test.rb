require 'test_helper'

describe TemperatureAdapter do
  describe 'temperature' do
    it 'should convert value properly' do
      @adapter = TemperatureAdapter.new(type: 'FarenheitTemperature', value: 98.6, convert_to: 'to_celsius')
      @adapter.temperature.class.name.must_equal 'FarenheitTemperature'
      @adapter.temperature.value.must_equal 98.6
    end
  end
end
