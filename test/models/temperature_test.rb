require 'test_helper'

describe Temperature do
  before do
    @temperature = Temperature.new(type: 'CelsiusTemperature', value: 20)
  end

  it 'should be valid' do
    @temperature.valid? true
  end

  describe 'value' do
    it 'should show error if nil' do
      @temperature.value = nil
      @temperature.valid? false
      @temperature.errors[:value].must_equal ["can't be blank"]
    end
  end

  describe 'type' do
    it 'should show error if nil' do
      @temperature.type = nil
      @temperature.valid? false
      @temperature.errors[:type].must_equal ["can't be blank"]
    end
  end

  describe 'type' do
    it 'should show error if empty' do
      @temperature.convert_to = ''
      @temperature.valid? false
      @temperature.errors[:convert_to].must_equal ["can't be blank", "is not included in the list"]
    end

    it 'should not show error if nil' do
      @temperature.convert_to = nil
      @temperature.valid? true
      @temperature.errors[:convert_to].must_equal []
    end
  end
end
