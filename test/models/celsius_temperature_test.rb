require 'test_helper'

describe CelsiusTemperature do
  before do
    @temperature = CelsiusTemperature.new(type: 'CelsiusTemperature', value: 20, convert_to: 'to_farenheit')
  end

  describe 'to_farenheit' do
    it 'should convert value properly' do
      @temperature.to_farenheit.value.must_equal 68.0
    end
  end

  describe 'value_text' do
    it 'should return text' do
      @temperature.value_text.must_equal '20 &deg; C'
    end
  end

  describe 'converted_value_text' do
    it 'should return converted value text' do
      @temperature.converted_value_text.must_equal '68.0 &deg; F'
    end
  end
end
