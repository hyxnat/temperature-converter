require 'test_helper'

describe FarenheitTemperature do
  before do
    @temperature = FarenheitTemperature.new(type: 'FarenheitTemperature', value: 98.6, convert_to: 'to_celsius')
  end

  describe 'to_farenheit' do
    it 'should convert value properly' do
      @temperature.to_celsius.value.must_equal 37.0
    end
  end

  describe 'value_text' do
    it 'should return text' do
      @temperature.value_text.must_equal '98.6 &deg; F'
    end
  end

  describe 'converted_value_text' do
    it 'should return converted value text' do
      @temperature.converted_value_text.must_equal '37.0 &deg; C'
    end
  end
end
