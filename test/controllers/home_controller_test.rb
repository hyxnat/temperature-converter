require 'test_helper'

describe 'HomeController' do
  describe 'index' do
    it 'should return celsius temperture record' do
      get :index

      must_respond_with :ok
      assigns(:temperature_adapter).temperature.value.must_equal 0
      assigns(:temperature_adapter).temperature.class.name.must_equal 'CelsiusTemperature'
    end
  end

  describe 'convert' do
    it 'should return farenheit temperture' do
      post :convert, temperature_adapter: { type: 'CelsiusTemperature', value: 37, convert_to: 'to_farenheit' }

      must_respond_with :ok
      assigns(:notice).must_equal '37 &deg; C is 98.6 &deg; F'
    end

    it 'should return error' do
      post :convert, temperature_adapter: { type: 'CelsiusTemperature', value: '', convert_to: 'to_farenheit' }

      must_respond_with :ok
      assigns(:error).must_equal 'Please enter the proper values'
    end
  end
end
